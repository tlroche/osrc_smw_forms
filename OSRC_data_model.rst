+++++++++++++++++++++++++++++++++
data model for OSRC documentation
+++++++++++++++++++++++++++++++++

.. contents:: **Table of Contents**

summary
+++++++

Following is a data model or data schema overview for capturing data about annual Open Source Report Cards (|OSRC|_\ s): see (e.g.) `Boundless' OSRC`_, `Google's OSRC`_. This doc seeks to capture my intentions regarding what I believe needs done: first in more traditional DBA/SQL terms (to the extent that I understand them, which may be insufficient), and eventually in more "executable" `Page Forms`_ (*PF*) and `Page Schemas`_ (*PS*) {property, type, category, template} semantics (as I learn them).

.. _Semantic MediaWiki: https://en.wikipedia.org/wiki/Semantic_MediaWiki
.. _SMW: https://en.wikipedia.org/wiki/Semantic_MediaWiki
.. _Page Forms: https://www.mediawiki.org/wiki/Extension:Page_Forms
.. _PF: https://www.mediawiki.org/wiki/Extension:Page_Forms
.. _Page Schemas: https://www.mediawiki.org/wiki/Extension:Page_Schemas
.. _PS: https://www.mediawiki.org/wiki/Extension:Page_Schemas

.. OSRC examples
.. _Boundless' OSRC: https://boundlessgeo.com/2016/11/boundless-open-source-report-card/
.. _Google's OSRC:  https://opensource.googleblog.com/2016/10/google-open-source-report-card.html

.. howto style an *internal* anchor/link (e.g., make it italic)
.. |Org| replace:: *Org*
.. DO NOT also do `_Org: #Org`: see https://sourceforge.net/p/doc
.. unstyled internal anchor/link:
.. _Orgs: #Org

.. styled internal anchor/link:
.. |OSRC| replace:: *OSRC*
.. unstyled:
.. _OSRCs: #OSRC

.. styled internal anchor/link. Note linkage does 'os-event' -> 'os-event' (at least in `restview`)
.. |OS_event| replace:: *OS_event*
.. unstyled:
.. _OS_events: #os-event

.. styled internal anchor/link:
.. |OS_project_by_year| replace:: *OS_project_by_year*
.. unstyled:
.. _OS_project_by_year's: #OS_project_by_year

datastructures
++++++++++++++

In following,

* ``datastructure`` ~= class, table
* ``pkey`` == 'primary key', ``fkey`` == 'foreign key'
* ``property`` ~= field, slot, attribute
* ``input type`` of a property refers to the kind of webform UI that `PF`_ will render for the user in order to get input for that property.
* ``data type`` of a property refers to a `PF datatype <https://www.semantic-mediawiki.org/wiki/Category:Datatypes>`_ (which names I will use when I believe I understand them); roughly, these are the types deployed by `PF`_ after it has consumed the input. For a map of data types to input types, see `Allowed input types for data types <https://www.mediawiki.org/wiki/Extension:Page_Forms/Input_types#Allowed_input_types_for_data_types>`_.
* null values disallowed unless explicitly permitted (``mandatory`` in `PS`_ semantics)

temporality
===========

One main difference between the datastructures is temporal scope:

* |OSRC|_\ s have annual scope: at least currently, the convention is to report an OSRC for one's efforts for a particular year, so an OSRC has a property name=\ ``OSRC_year``. ICBW, and even if IIUC that may change, but that's how it seems now.
* |OS_event|_\ s have explicit start- and end-date properties.
* In ordinary usage, an "OS project" has variable temporal scope. But for this app, given their tie to *OSRC*\ s, |OS_project_by_year|_\ s must have annual scope. (At least until I find a way around OSRC annuality, or that ceases to be true.)
* |Org|_\ s are eternal (of course :-) though their attributes may change.

Org
===

``Org`` is short for "organisation" or "organization," and is intended to avoid that spelling annoyance (the wiki's host is a global ... one of these :-) and extra typing. These ``Org``\ s are allies or members of the wiki host, and we are here only capturing a subset of their information. (This will be merged with another {table, set of forms} regarding memberships, but I was asked to keep down the number of fields in the forms for this app.)

`OSRCs`_ are really the primary datastructure for this "app," but ``Org``\ s are central, since ``org_name`` is key (pkey or fkey) for the rest of the app.

=======================  =======================================================
property
--------------------------------------------------------------------------------
name                     description
=======================  =======================================================
``org_name``             {page title, pkey}, type=\ ``Page``
``org_country``          data type=\ ``String``, input type=text (**TODO:** enum from dropdown of country names)
``org_website``          data type=\ ``URL``, input type=text, null allowed
``org_contact_name``     data type=\ ``String``, input type=text
``org_contact_email``    data type=\ ``Email``, input type=text
``org_map``              data type=\ ``Geographic coordinate``, input type=openlayers, null allowed
``org_OSRCs``            list all ``OSRC``\ s for the ``Org`` (null allowed for startup): aggregate on ``OSRC`` fkey=\ ``OSRC_host-org_name``
=======================  =======================================================

OSRC
====

Open Source Report Cards (*OSRC*\ s) capture information about an `Org`_\ 's open-source efforts for a given year: see (e.g.) `Boundless' OSRC`_, `Google's OSRC`_. Note the ambiguity here: a larger ``Org`` will typically do its own pretty doc/webpage (captured here by property=\ ``OSRC_year_document_URI``), which this app is intended to represent if available (and to query and otherwise manipulate). Smaller ``Org``\ s might only fill out these forms, however.

===========================  =======================================================
property
------------------------------------------------------------------------------------
name                         description
===========================  =======================================================
``OSRC_org_name_and_year``   page title, catenated pkey='${``OSRC_host-org_name``} ${``OSRC_year``}'
``OSRC_host-org_name``       fkey on ``Org`` property=\ ``org_name``
``OSRC_year``                data type=\ ``Date``, input type=year
``OSRC_plan_for_next_year``  data type=\ ``String``, input type=textarea, null allowed
``OSRC_year_document_URI``   data type=\ ``URL``, input type=text, null allowed
``OSRC_events_this_year``    list all ``OS_event``\ s for this ``Org`` this year (null allowed): aggregate on ``${OS_event_host-org_name} && ${OS_event_year}``
``OSRC_projects_this_year``  list all ``OS_project``\ s for this ``Org`` this year (null allowed): aggregate on ``${OS_project_sponsor_org_name} && ${OS_project_year}``
===========================  =======================================================

OS_event
========

Represents an open-source-oriented event. For current purposes,

* |Org|_\ s are one-to-many in ``OS_event``\ s: no current provision for multi-hosted events. (That may need to change, but I'm trying to keep this simple for now.)
* ``OS_event``\ s have explicit start- and end-dates, which are expected to map to a single year (for |OSRC|_\ s).
* Like ``Org``\ s, an ``OS_event`` may have its own document, webpage, etc.

===========================  =======================================================
property
------------------------------------------------------------------------------------
name                         description
===========================  =======================================================
``OS_event_name_and_year``   page title, catenated pkey='${``OSRC_event_host-org_name``} ${``OSRC_event_year``}'
``OS_event_host-org_name``   fkey on ``Org`` property=\ ``org_name``
``OS_event_name``            data type=\ ``String``, input type=text
``OS_event_start_date``      data type=\ ``Date``, input type=date
``OS_event_end_date``        data type=\ ``Date``, input type=date
``OS_event_year``            data type=\ ``Date`` (**TODO:** compute from previous valid ``Date``\ s)
``OS_event_document_URI``    data type=\ ``URL``, input type=text, null allowed
``OS_event_description``     data type=\ ``String``, input type=textarea
===========================  =======================================================

OS_project_by_year
==================

Represents an open-source-oriented project. For current purposes,

* |Org|_\ s are one-to-many in ``OS_project_by_year``\ s: no current provision for multi-sponsored projects. (That may need to change, but I'm trying to keep this simple for now.)
* Like ``Org``\ s and `OS_event`_\ s, an ``OS_project_by_year`` may have its own document, webpage, etc.

===============================  =======================================================
property
----------------------------------------------------------------------------------------
name                             description
===============================  =======================================================
``OS_project_name_and_year``     page title, catenated pkey='${``OS_project_name``} ${``OS_project_year``}'
``OS_project_sponsor_org_name``  fkey on ``Org`` property=\ ``org_name``
``OS_project_name``              data type=\ ``String``, input type=text
``OS_project_year``              data type=\ ``Date``, input type=year
``OS_project_document_URI``      data type=\ ``URL``, input type=text, null allowed
``OS_project_description``       data type=\ ``String``, input type=textarea
===============================  =======================================================

queries
+++++++

I've asked, and for now the wiki-hosting group seems to want support for only the following queries:

1. `Orgs`_ : i.e., for what affiliated ``Org``\ s do we have data? I suspect this will require a separate query page (just aggregate on property=\ ``org_name``), but ICBW.
#. `OSRCs`_ by ``Org``: i.e., show all the ``OSRC``\ s created by/for the ``Org``. This should be available via the ``Org``\ 's URI, e.g., the page with title=\ ``org_name``.
#. `OSRCs`_ by ``Org`` and year. For now, just get via the ``Org``\ 's page (in browser) or URI (via API).
#. ``OSRC``\ s by year: i.e., show all the ``OSRC``\ s created for that year. Will require a separate query page (IIUC), just fill-in year.
#. `OS_event`_\ s for ``Org`` + year: available via the ``OSRC``\ 's page/URI.
#. ``OS_event``\ s for year: will require a separate query page (IIUC), just fill-in year.
#. `OS_project_by_year`_\ s for ``Org`` + year: available via the ``OSRC``\ 's page/URI.
#. ``OS project_by_year``\ s for year: will require a separate query page (IIUC), just fill-in year.
