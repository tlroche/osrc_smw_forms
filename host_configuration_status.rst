+++++++++++++++++++++++++++++
SMW host configuration status
+++++++++++++++++++++++++++++

.. contents:: **Table of Contents**

summary
+++++++

As of 17 Nov 2016, several of the components of the target `Semantic MediaWiki`_ (*SMW*) host's platform are significantly downlevel WRT latest security updates, not to mention latest SMW framework extensions. I'd like to see that work done while I'm prototyping offline (to prevent the new work interfering with old work) on the `Referata scratchpad`_ or other `SMW/PF freehost`_.

.. _Semantic MediaWiki: https://en.wikipedia.org/wiki/Semantic_MediaWiki
.. _SMW: https://en.wikipedia.org/wiki/Semantic_MediaWiki
.. _Page Forms: https://www.mediawiki.org/wiki/Extension:Page_Forms
.. _Referata scratchpad: http://scratchpad.referata.com/wiki/Main_Page
.. _SMW/PF freehost: https://www.mediawiki.org/wiki/Extension:Page_Forms/Hosting

notes on versions
+++++++++++++++++

glossary
========

* *CLV*: current latest version. Without further refinement, *CLV* may be development, stable, or secure.
* *CLSV* can mean either

    * *CLSeV*: current latest **secure** version. Obviously that will change over time.
    * *CLStV*: current latest **stable** version. (Ditto).

datasources
===========

* Currently-installed versions are taken from the SMW's ``Special:Version`` page where available.
* MediaWiki typically lists CLSeV at/near top of its `Recent News <https://www.mediawiki.org/wiki/News#Recent_news>`_.
* PHP typically lists CLSeV at/near top of its *News Archive* (currently `here <https://secure.php.net/archive/2016.php>`_).
* For MySQL, one must typically work backward from one's package manager. Presuming this SMW host runs Debian, one can search for CLSV of (e.g.) package=\ ``mysql-server``. If, e.g., one is running Debian stable, one can search on its `latest release <https://packages.debian.org/stable/mysql-server>`_ in that stream.

    * Alternatively, one can search through the notes for each MySQL major release (e.g., for `5.7 <https://dev.mysql.com/doc/relnotes/mysql/5.7/en/>`_), but Oracle does not provide an easy way to do this (that I can see, YMMV).

* SMW lists CLV (only, not CLSeV) near top of `​its page <https://www.semantic-mediawiki.org/wiki/Semantic_MediaWiki>`_.
* The SMW extension formerly known as *Semantic Forms* is now known as *Page Forms*, and lists CLV in the infobox near top right of `​its page <https://www.mediawiki.org/wiki/Extension:Page_Forms>`_.
* The SMW extension formerly known as *Semantic Maps* is now known as *Maps*, and lists CLStV near top of its `​release notes <https://github.com/JeroenDeDauw/Maps/blob/master/RELEASE-NOTES.md>`_.
* Semantic Result Formats lists latest stable version near top of its `release notes <https://github.com/SemanticMediaWiki/SemanticResultFormats/blob/master/RELEASE-NOTES.md>`_.

current install status
++++++++++++++++++++++

=======================  ==================  ==================================================
component                version
-----------------------  ----------------------------------------------------------------------
name                     installed           notes
=======================  ==================  ==================================================
Debian                   6/Squeeze           I don't have access to check the platform, but am guessing from installed-version names reported below. `CLSeV`_\ =8/Jessie: 6 is not even oldstable currently.
PHP                      5.3.3-7+squeeze29   CLSeV=5.6.28
MySQL                    5.1.73-1+deb6u1     `Debian stable CLSeV <https://packages.debian.org/stable/mysql-server>`_\ =5.5.53-0+deb8u1
MediaWiki                1.25.3              CLSeV=1.27.1 (also available via `Debian backports <https://packages.debian.org/stable-backports/mediawiki>`_)
Semantic MediaWiki       2.3                 `CLStV`_\ =2.4.2
Semantic Forms           3.4                 CLStV=4.0.2
Semantic Maps            3.2                 CLStV=4.0
Semantic Result Formats  2.3                 CLStV=2.4.0
=======================  ==================  ==================================================

.. _CLSeV: #glossary
.. _CLStV: #glossary

current desired status
++++++++++++++++++++++

=======================  ==================  ==================================================
component                version
-----------------------  ----------------------------------------------------------------------
name                     target              notes
=======================  ==================  ==================================================
Debian                   8/Jessie            `CLSeV`_
PHP                      5.6.28              CLSeV
MySQL                    5.5.53-0+deb8u1     `Debian stable CLSeV <https://packages.debian.org/stable/mysql-server>`_
MediaWiki                1.27.1              CLSeV
Semantic MediaWiki       2.4.2               `CLStV`_, see `compatibility <https://github.com/SemanticMediaWiki/SemanticMediaWiki/blob/master/docs/COMPATIBILITY.md#platform-compatibility>`_
Semantic Forms           4.0.2               CLStV, compatible with MW >= 1.21
Semantic Maps            4.0                 CLStV, see `compatibility <https://github.com/JeroenDeDauw/Maps/blob/master/INSTALL.md#platform-compatibility-and-release-status>`_
Semantic Result Formats  2.4.0               CLStV, see `Requirements <https://github.com/SemanticMediaWiki/SemanticResultFormats#requirements>`_
=======================  ==================  ==================================================
