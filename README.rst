+++++++++++++++++++++++++++++++++++++
SMW Page Forms for OSRC documentation
+++++++++++++++++++++++++++++++++++++

Toward development of a `Page Forms`_ (*PF*) "app" for capturing data about annual Open Source Report Cards (*OSRC*, see examples from `Boundless <https://boundlessgeo.com/2016/11/boundless-open-source-report-card/>`_ and `Google <https://opensource.googleblog.com/2016/10/google-open-source-report-card.html>`_). Planned sub-{pages, sections}:

0. `component updates <./host_configuration_status.rst>`_ that will eventually (hopefully soonest--many are security-related) need done to the current SMW host. This is not blocking on initial work, at least for now, since prototyping can be done on the `Referata scratchpad`_, `SMW sandbox`_, or other `SMW/PF freehost`_.
#. `overview <./OSRC_data_model.rst>`_ of data model (including queries)

    * start as DBA/SQL-style data schema
    * add PF semantics for {properties, categories, templates, forms} as I learn PF
    * ... and `Page Schemas`_ (PS) semantics if useful

#. deploy-automation models and code (using, e.g., PS, `Mobo`_), supplemented with procedural documentation for manual steps where unavoidable.

    * `PS schema XML in process <./OSRC_app__Page_Schemas.xml>`_

.. _Semantic MediaWiki: https://en.wikipedia.org/wiki/Semantic_MediaWiki
.. _SMW: https://en.wikipedia.org/wiki/Semantic_MediaWiki
.. _Page Forms: https://www.mediawiki.org/wiki/Extension:Page_Forms
.. _Page Schemas: https://www.mediawiki.org/wiki/Extension:Page_Schemas
.. _Referata scratchpad: http://scratchpad.referata.com/wiki/Main_Page
.. _SMW sandbox: https://sandbox.semantic-mediawiki.org/wiki/
.. _SMW/PF freehost: https://www.mediawiki.org/wiki/Extension:Page_Forms/Hosting
.. _Mobo: https://www.npmjs.com/package/mobo
